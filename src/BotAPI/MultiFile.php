<?php

namespace TGBotBase\BotAPI;

//TODO: make this work

class MultiFile {

    private $files;

    public function __construct(array $files)
    {
        foreach ($files as $file) {
            $this->files[$file] = new File($file);
        }
    }

    public function getFiles()
    {
        return $this->files;
    }
}
