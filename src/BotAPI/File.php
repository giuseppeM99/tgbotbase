<?php

namespace TGBotBase\BotAPI;

class File {

    private static function encodeMultipart($file, $filename)
    {
        //TODO
    }

    private $file;

    public function __construct(string $file, string $filename = NULL)
    {

        if (\filter_var($file, FILTER_VALIDATE_URL)) {
            $this->file = $file;
        } elseif (ctype_print($file)) {
            $this->file = new \CURLFile($file, \mime_content_type($file), $filename ?: $file);
        } elseif (!empty($filename)) {
            $file = self::encodeMultipart($file, $filename);
        } else {
            throw new \TGBotBase\Exception('No filename');
        }
    }

    public function getFile()
    {
        return $this->file;
    }
}
