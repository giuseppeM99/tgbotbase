<?php

namespace TGBotBase\BotAPI;

class Request
{
    private static $baseClient;

    private static function getBaseClient()
    {
        self::initBaseClient();
        return self::$baseClient;
    }

    private static function initBaseClient()
    {
        if (self::$baseClient === NULL) {
            self::$baseClient = \TGBotBase\HTTPClient\Client::newInstance(\TGBotBase\Config::getConfig('TELEGRAM_API_URL'));
        }
    }

    private static function checkParams($params)
    {
        foreach ($params as $k => &$v) {

            if ($v instanceof File) {
                $v = $v->getFile();
            } elseif ($v instanceof Media) {
                $v = $v->getMedia();
            }

            if (is_array($v)) {
                $v = \json_encode($v);
            }
        }

        return $params;
    }


    private $client;
    private $token;

    private function createClient()
    {
        $this->client = new \TGBotBase\HTTPClient\Client(\TGBotBase\Config::getConfig('TELEGRAM_API_URL')."bot$this->token/");

        if (\TGBotBase\Config::getConfig('SHARED_INSTANCE', true)) {
            $this->client->setCustomInstance(self::getBaseClient());
        }
    }

    public function __construct(string $token)
    {
        $this->token = $token;
        $this->createClient();
    }

    public function get(string $path = '', array $params = [])
    {
        return $this->client->get($path, $params);
    }

    public function post(string $path = '', array $params = [])
    {
        return $this->client->post($path, self::checkParams($params));
    }

    public function __destruct()
    {
    }
}
