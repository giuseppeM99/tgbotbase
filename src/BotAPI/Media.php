<?php

namespace TGBotBase\BotAPI;

class Media {

    private $media;

    public function __construct($media)
    {
        $this->media = $media;
    }

    public function getMedia()
    {
        return $this->media;
    }
}
