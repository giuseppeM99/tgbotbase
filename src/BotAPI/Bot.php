<?php

namespace TGBotBase\BotAPI;

class Bot extends Request
{
    private $token;
    private $ID;

    public function __construct($token)
    {
        $this->token = $token;
        $this->ID = explode(':', $token)[0];
        parent::__construct($token);
    }

    public function getID()
    {
        return $this->ID;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getUpdates(
        int $offset = null,
        int $limit = null,
        int $timeout = null,
        array $allowed_updates = null
    ) {
        $args = [];
        if ($offset !== null) {
            $args['offset'] = $offset;
        }
        if ($limit !== null) {
            $args['limit'] = $limit;
        }
        if ($timeout !== null) {
            $args['timeout'] = $timeout;
        }
        if ($allowed_updates !== null) {
            $args['allowed_updates'] = $allowed_updates;
        }
        return $this->post('getUpdates', $args);
    }

    public function setWebhook(
        string $url,
        $certificate = null,
        int $max_connections = null,
        array $allowed_updates = null
    ) {
        $args = [
            'url' => $url
        ];
        if ($certificate !== null) {
            $args['certificate'] = new File($certificate);
        }
        if ($max_connections !== null) {
            $args['max_connections'] = $max_connections;
        }
        if ($allowed_updates !== null) {
            $args['allowed_updates'] = $allowed_updates;
        }
        return $this->post('setWebhook', $args);
    }

    public function deleteWebhook()
    {
        return $this->get('deleteWebhook');
    }

    public function getWebhookInfo()
    {
        return $this->get('getWebhookInfo');
    }

    public function getMe()
    {
        return $this->get('getMe');
    }

    public function sendMessage(
        $chat_id,
        string $text,
        string $parse_mode = null,
        bool $disable_web_page_preview = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'text'    => $text
        ];
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($disable_web_page_preview !== null) {
            $args['disable_web_page_preview'] = $disable_web_page_preview;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendMessage', $args);
    }

    public function forwardMessage(
        $chat_id,
        $from_chat_id,
        int $message_id,
        bool $disable_notification = null
    ) {
        $args = [
            'chat_id'      => $chat_id,
            'from_chat_id' => $from_chat_id,
            'message_id'   => $message_id
        ];
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        return $this->post('forwardMessage', $args);
    }

    public function sendPhoto(
        $chat_id,
        $photo,
        string $caption = null,
        string $parse_mode = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'photo'   => new File($photo)
        ];
        if ($caption !== null) {
            $args['caption'] = $caption;
        }
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendPhoto', $args);
    }

    public function sendAudio(
        $chat_id,
        $audio,
        string $caption = null,
        string $parse_mode = null,
        int $duration = null,
        string $performer = null,
        string $title = null,
        $thumb = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'audio'   => new File($audio)
        ];
        if ($caption !== null) {
            $args['caption'] = $caption;
        }
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($duration !== null) {
            $args['duration'] = $duration;
        }
        if ($performer !== null) {
            $args['performer'] = $performer;
        }
        if ($title !== null) {
            $args['title'] = $title;
        }
        if ($thumb !== null) {
            $args['thumb'] = $thumb;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendAudio', $args);
    }

    public function sendDocument(
        $chat_id,
        $document,
        string $caption = null,
        string $parse_mode = null,
        $thumb = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'  => $chat_id,
            'document' => new File($document)
        ];
        if ($thumb !== null) {
            $args['thumb'] = $thumb;
        }
        if ($caption !== null) {
            $args['caption'] = $caption;
        }
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendDocument', $args);
    }

    public function sendVideo(
        $chat_id,
        $video,
        string $caption = null,
        string $parse_mode = null,
        int $duration = null,
        int $width = null,
        int $height = null,
        $thumb = null,
        bool $supports_streaming = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'video'   => new File($video)
        ];
        if ($duration !== null) {
            $args['duration'] = $duration;
        }
        if ($width !== null) {
            $args['width'] = $width;
        }
        if ($height !== null) {
            $args['height'] = $height;
        }
        if ($thumb !== null) {
            $args['thumb'] = $thumb;
        }
        if ($caption !== null) {
            $args['caption'] = $caption;
        }
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($supports_streaming !== null) {
            $args['supports_streaming'] = $supports_streaming;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendVideo', $args);
    }

    public function sendAnimation(
        $chat_id,
        $animation,
        string $caption = null,
        string $parse_mode = null,
        int $duration = null,
        int $width = null,
        int $height = null,
        $thumb = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'   => $chat_id,
            'animation' => new File($animation)
        ];
        if ($duration !== null) {
            $args['duration'] = $duration;
        }
        if ($width !== null) {
            $args['width'] = $width;
        }
        if ($height !== null) {
            $args['height'] = $height;
        }
        if ($thumb !== null) {
            $args['thumb'] = $thumb;
        }
        if ($caption !== null) {
            $args['caption'] = $caption;
        }
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendAnimation', $args);
    }

    public function sendVoice(
        $chat_id,
        $voice,
        string $caption = null,
        string $parse_mode = null,
        int $duration = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'voice'   => new File($voice)
        ];
        if ($caption !== null) {
            $args['caption'] = $caption;
        }
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($duration !== null) {
            $args['duration'] = $duration;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendVoice', $args);
    }

    public function sendVideoNote(
        $chat_id,
        $video_note,
        int $duration = null,
        int $length = null,
        $thumb = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'    => $chat_id,
            'video_note' => new File($video_note)
        ];
        if ($duration !== null) {
            $args['duration'] = $duration;
        }
        if ($length !== null) {
            $args['length'] = $length;
        }
        if ($thumb !== null) {
            $args['thumb'] = $thumb;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendVideoNote', $args);
    }

    public function sendMediaGroup(
        $chat_id,
        array $media,
        bool $disable_notification = null,
        int $reply_to_message_id = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'media'   => new MultiFile($media)
        ];
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        return $this->post('sendMediaGroup', $args);
    }

    public function sendLocation(
        $chat_id,
        float $latitude,
        float $longitude,
        int $live_period = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'   => $chat_id,
            'latitude'  => $latitude,
            'longitude' => $longitude
        ];
        if ($live_period !== null) {
            $args['live_period'] = $live_period;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendLocation', $args);
    }

    public function editMessageLiveLocation(
        $chat_id = null,
        int $message_id = null,
        string $inline_message_id = null,
        float $latitude,
        float $longitude,
        array $reply_markup = null
    ) {
        $args = [
            'latitude'  => $latitude,
            'longitude' => $longitude
        ];
        if ($chat_id !== null) {
            $args['chat_id'] = $chat_id;
        }
        if ($message_id !== null) {
            $args['message_id'] = $message_id;
        }
        if ($inline_message_id !== null) {
            $args['inline_message_id'] = $inline_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('editMessageLiveLocation', $args);
    }

    public function stopMessageLiveLocation(
        $chat_id = null,
        int $message_id = null,
        string $inline_message_id = null,
        array $reply_markup = null
    ) {
        $args = [];
        if ($chat_id !== null) {
            $args['chat_id'] = $chat_id;
        }
        if ($message_id !== null) {
            $args['message_id'] = $message_id;
        }
        if ($inline_message_id !== null) {
            $args['inline_message_id'] = $inline_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('stopMessageLiveLocation', $args);
    }

    public function sendVenue(
        $chat_id,
        float $latitude,
        float $longitude,
        string $title,
        string $address,
        string $foursquare_id = null,
        string $foursquare_type = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'   => $chat_id,
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'title'     => $title,
            'address'   => $address
        ];
        if ($foursquare_id !== null) {
            $args['foursquare_id'] = $foursquare_id;
        }
        if ($foursquare_type !== null) {
            $args['foursquare_type'] = $foursquare_type;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendVenue', $args);
    }

    public function sendContact(
        $chat_id,
        string $phone_number,
        string $first_name,
        string $last_name = null,
        string $vcard = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'      => $chat_id,
            'phone_number' => $phone_number,
            'first_name'   => $first_name
        ];
        if ($last_name !== null) {
            $args['last_name'] = $last_name;
        }
        if ($vcard !== null) {
            $args['vcard'] = $vcard;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendContact', $args);
    }

    public function sendPoll(
        $chat_id,
        string $question,
        array $options,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'  => $chat_id,
            'question' => $question,
            'options'  => $options
        ];
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendPoll', $args);
    }

    public function sendChatAction(
        $chat_id,
        string $action
    ) {
        $args = [
            'chat_id' => $chat_id,
            'action'  => $action
        ];
        return $this->post('sendChatAction', $args);
    }

    public function getUserProfilePhotos(
        int $user_id,
        int $offset = null,
        int $limit = null
    ) {
        $args = [
            'user_id' => $user_id
        ];
        if ($offset !== null) {
            $args['offset'] = $offset;
        }
        if ($limit !== null) {
            $args['limit'] = $limit;
        }
        return $this->post('getUserProfilePhotos', $args);
    }

    public function getFile(
        string $file_id
    ) {
        $args = [
            'file_id' => $file_id
        ];
        return $this->post('getFile', $args);
    }

    public function kickChatMember(
        $chat_id,
        int $user_id,
        int $until_date = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'user_id' => $user_id
        ];
        if ($until_date !== null) {
            $args['until_date'] = $until_date;
        }
        return $this->post('kickChatMember', $args);
    }

    public function unbanChatMember(
        $chat_id,
        int $user_id
    ) {
        $args = [
            'chat_id' => $chat_id,
            'user_id' => $user_id
        ];
        return $this->post('unbanChatMember', $args);
    }

    public function restrictChatMember(
        $chat_id,
        int $user_id,
        array $permissions,
        int $until_date = null
    ) {
        $args = [
            'chat_id'     => $chat_id,
            'user_id'     => $user_id,
            'permissions' => $permissions
        ];
        if ($until_date !== null) {
            $args['until_date'] = $until_date;
        }
        return $this->post('restrictChatMember', $args);
    }

    public function promoteChatMember(
        $chat_id,
        int $user_id,
        bool $can_change_info = null,
        bool $can_post_messages = null,
        bool $can_edit_messages = null,
        bool $can_delete_messages = null,
        bool $can_invite_users = null,
        bool $can_restrict_members = null,
        bool $can_pin_messages = null,
        bool $can_promote_members = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'user_id' => $user_id
        ];
        if ($can_change_info !== null) {
            $args['can_change_info'] = $can_change_info;
        }
        if ($can_post_messages !== null) {
            $args['can_post_messages'] = $can_post_messages;
        }
        if ($can_edit_messages !== null) {
            $args['can_edit_messages'] = $can_edit_messages;
        }
        if ($can_delete_messages !== null) {
            $args['can_delete_messages'] = $can_delete_messages;
        }
        if ($can_invite_users !== null) {
            $args['can_invite_users'] = $can_invite_users;
        }
        if ($can_restrict_members !== null) {
            $args['can_restrict_members'] = $can_restrict_members;
        }
        if ($can_pin_messages !== null) {
            $args['can_pin_messages'] = $can_pin_messages;
        }
        if ($can_promote_members !== null) {
            $args['can_promote_members'] = $can_promote_members;
        }
        return $this->post('promoteChatMember', $args);
    }

    public function setChatPermissions(
        $chat_id,
        array $permissions
    ) {
        $args = [
            'chat_id'     => $chat_id,
            'permissions' => $permissions
        ];
        return $this->post('setChatPermissions', $args);
    }

    public function exportChatInviteLink(
        $chat_id
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        return $this->post('exportChatInviteLink', $args);
    }

    public function setChatPhoto(
        $chat_id,
        $photo
    ) {
        $args = [
            'chat_id' => $chat_id,
            'photo'   => new File($photo)
        ];
        return $this->post('setChatPhoto', $args);
    }

    public function deleteChatPhoto(
        $chat_id
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        return $this->post('deleteChatPhoto', $args);
    }

    public function setChatTitle(
        $chat_id,
        string $title
    ) {
        $args = [
            'chat_id' => $chat_id,
            'title'   => $title
        ];
        return $this->post('setChatTitle', $args);
    }

    public function setChatDescription(
        $chat_id,
        string $description = null
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        if ($description !== null) {
            $args['description'] = $description;
        }
        return $this->post('setChatDescription', $args);
    }

    public function pinChatMessage(
        $chat_id,
        int $message_id,
        bool $disable_notification = null
    ) {
        $args = [
            'chat_id'    => $chat_id,
            'message_id' => $message_id
        ];
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        return $this->post('pinChatMessage', $args);
    }

    public function unpinChatMessage(
        $chat_id
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        return $this->post('unpinChatMessage', $args);
    }

    public function leaveChat(
        $chat_id
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        return $this->post('leaveChat', $args);
    }

    public function getChat(
        $chat_id
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        return $this->post('getChat', $args);
    }

    public function getChatAdministrators(
        $chat_id
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        return $this->post('getChatAdministrators', $args);
    }

    public function getChatMembersCount(
        $chat_id
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        return $this->post('getChatMembersCount', $args);
    }

    public function getChatMember(
        $chat_id,
        int $user_id
    ) {
        $args = [
            'chat_id' => $chat_id,
            'user_id' => $user_id
        ];
        return $this->post('getChatMember', $args);
    }

    public function setChatStickerSet(
        $chat_id,
        string $sticker_set_name
    ) {
        $args = [
            'chat_id'          => $chat_id,
            'sticker_set_name' => $sticker_set_name
        ];
        return $this->post('setChatStickerSet', $args);
    }

    public function deleteChatStickerSet(
        $chat_id
    ) {
        $args = [
            'chat_id' => $chat_id
        ];
        return $this->post('deleteChatStickerSet', $args);
    }

    public function answerCallbackQuery(
        string $callback_query_id,
        string $text = null,
        bool $show_alert = null,
        string $url = null,
        int $cache_time = null
    ) {
        $args = [
            'callback_query_id' => $callback_query_id
        ];
        if ($text !== null) {
            $args['text'] = $text;
        }
        if ($show_alert !== null) {
            $args['show_alert'] = $show_alert;
        }
        if ($url !== null) {
            $args['url'] = $url;
        }
        if ($cache_time !== null) {
            $args['cache_time'] = $cache_time;
        }
        return $this->post('answerCallbackQuery', $args);
    }

    public function editMessageText(
        $chat_id = null,
        int $message_id = null,
        string $inline_message_id = null,
        string $text,
        string $parse_mode = null,
        bool $disable_web_page_preview = null,
        array $reply_markup = null
    ) {
        $args = [
            'text' => $text
        ];
        if ($chat_id !== null) {
            $args['chat_id'] = $chat_id;
        }
        if ($message_id !== null) {
            $args['message_id'] = $message_id;
        }
        if ($inline_message_id !== null) {
            $args['inline_message_id'] = $inline_message_id;
        }
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($disable_web_page_preview !== null) {
            $args['disable_web_page_preview'] = $disable_web_page_preview;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('editMessageText', $args);
    }

    public function editMessageCaption(
        $chat_id = null,
        int $message_id = null,
        string $inline_message_id = null,
        string $caption = null,
        string $parse_mode = null,
        array $reply_markup = null
    ) {
        $args = [];
        if ($chat_id !== null) {
            $args['chat_id'] = $chat_id;
        }
        if ($message_id !== null) {
            $args['message_id'] = $message_id;
        }
        if ($inline_message_id !== null) {
            $args['inline_message_id'] = $inline_message_id;
        }
        if ($caption !== null) {
            $args['caption'] = $caption;
        }
        if ($parse_mode !== null) {
            $args['parse_mode'] = $parse_mode;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('editMessageCaption', $args);
    }

    public function editMessageMedia(
        $chat_id = null,
        int $message_id = null,
        string $inline_message_id = null,
        $media,
        array $reply_markup = null
    ) {
        if (! $media instanceof Media) {
            $media = new Media($media);
        }
        $args = [
            'media' => $media
        ];
        if ($chat_id !== null) {
            $args['chat_id'] = $chat_id;
        }
        if ($message_id !== null) {
            $args['message_id'] = $message_id;
        }
        if ($inline_message_id !== null) {
            $args['inline_message_id'] = $inline_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('editMessageMedia', $args);
    }

    public function editMessageReplyMarkup(
        $chat_id = null,
        int $message_id = null,
        string $inline_message_id = null,
        array $reply_markup = null
    ) {
        $args = [];
        if ($chat_id !== null) {
            $args['chat_id'] = $chat_id;
        }
        if ($message_id !== null) {
            $args['message_id'] = $message_id;
        }
        if ($inline_message_id !== null) {
            $args['inline_message_id'] = $inline_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('editMessageReplyMarkup', $args);
    }

    public function stopPoll(
        $chat_id,
        int $message_id,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'    => $chat_id,
            'message_id' => $message_id
        ];
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('stopPoll', $args);
    }

    public function deleteMessage(
        $chat_id,
        int $message_id
    ) {
        $args = [
            'chat_id'    => $chat_id,
            'message_id' => $message_id
        ];
        return $this->post('deleteMessage', $args);
    }

    public function sendSticker(
        $chat_id,
        $sticker,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id' => $chat_id,
            'sticker' => $sticker
        ];
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendSticker', $args);
    }

    public function getStickerSet(
        string $name
    ) {
        $args = [
            'name' => $name
        ];
        return $this->post('getStickerSet', $args);
    }

    public function uploadStickerFile(
        int $user_id,
        $png_sticker
    ) {
        $args = [
            'user_id'     => $user_id,
            'png_sticker' => new File($png_sticker)
        ];
        return $this->post('uploadStickerFile', $args);
    }

    public function createNewStickerSet(
        int $user_id,
        string $name,
        string $title,
        $png_sticker,
        string $emojis,
        bool $contains_masks = null,
        array $mask_position = null
    ) {
        $args = [
            'user_id'     => $user_id,
            'name'        => $name,
            'title'       => $title,
            'png_sticker' => $png_sticker,
            'emojis'      => $emojis
        ];
        if ($contains_masks !== null) {
            $args['contains_masks'] = $contains_masks;
        }
        if ($mask_position !== null) {
            $args['mask_position'] = $mask_position;
        }
        return $this->post('createNewStickerSet', $args);
    }

    public function addStickerToSet(
        int $user_id,
        string $name,
        $png_sticker,
        string $emojis,
        array $mask_position = null
    ) {
        $args = [
            'user_id'     => $user_id,
            'name'        => $name,
            'png_sticker' => $png_sticker,
            'emojis'      => $emojis
        ];
        if ($mask_position !== null) {
            $args['mask_position'] = $mask_position;
        }
        return $this->post('addStickerToSet', $args);
    }

    public function setStickerPositionInSet(
        string $sticker,
        int $position
    ) {
        $args = [
            'sticker'  => $sticker,
            'position' => $position
        ];
        return $this->post('setStickerPositionInSet', $args);
    }

    public function deleteStickerFromSet(
        string $sticker
    ) {
        $args = [
            'sticker' => $sticker
        ];
        return $this->post('deleteStickerFromSet', $args);
    }

    public function answerInlineQuery(
        string $inline_query_id,
        array $results,
        int $cache_time = null,
        bool $is_personal = null,
        string $next_offset = null,
        string $switch_pm_text = null,
        string $switch_pm_parameter = null
    ) {
        $args = [
            'inline_query_id' => $inline_query_id,
            'results'         => $results
        ];
        if ($cache_time !== null) {
            $args['cache_time'] = $cache_time;
        }
        if ($is_personal !== null) {
            $args['is_personal'] = $is_personal;
        }
        if ($next_offset !== null) {
            $args['next_offset'] = $next_offset;
        }
        if ($switch_pm_text !== null) {
            $args['switch_pm_text'] = $switch_pm_text;
        }
        if ($switch_pm_parameter !== null) {
            $args['switch_pm_parameter'] = $switch_pm_parameter;
        }
        return $this->post('answerInlineQuery', $args);
    }

    public function sendInvoice(
        int $chat_id,
        string $title,
        string $description,
        string $payload,
        string $provider_token,
        string $start_parameter,
        string $currency,
        $prices,
        array $provider_data = null,
        string $photo_url = null,
        int $photo_size = null,
        int $photo_width = null,
        int $photo_height = null,
        bool $need_name = null,
        bool $need_phone_number = null,
        bool $need_email = null,
        bool $need_shipping_address = null,
        bool $send_phone_number_to_provider = null,
        bool $send_email_to_provider = null,
        bool $is_flexible = null,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'         => $chat_id,
            'title'           => $title,
            'description'     => $description,
            'payload'         => $payload,
            'provider_token'  => $provider_token,
            'start_parameter' => $start_parameter,
            'currency'        => $currency,
            'prices'          => $prices
        ];
        if ($provider_data !== null) {
            $args['provider_data'] = $provider_data;
        }
        if ($photo_url !== null) {
            $args['photo_url'] = $photo_url;
        }
        if ($photo_size !== null) {
            $args['photo_size'] = $photo_size;
        }
        if ($photo_width !== null) {
            $args['photo_width'] = $photo_width;
        }
        if ($photo_height !== null) {
            $args['photo_height'] = $photo_height;
        }
        if ($need_name !== null) {
            $args['need_name'] = $need_name;
        }
        if ($need_phone_number !== null) {
            $args['need_phone_number'] = $need_phone_number;
        }
        if ($need_email !== null) {
            $args['need_email'] = $need_email;
        }
        if ($need_shipping_address !== null) {
            $args['need_shipping_address'] = $need_shipping_address;
        }
        if ($send_phone_number_to_provider !== null) {
            $args['send_phone_number_to_provider'] = $send_phone_number_to_provider;
        }
        if ($send_email_to_provider !== null) {
            $args['send_email_to_provider'] = $send_email_to_provider;
        }
        if ($is_flexible !== null) {
            $args['is_flexible'] = $is_flexible;
        }
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendInvoice', $args);
    }

    public function answerShippingQuery(
        array $shipping_query_id,
        bool $ok,
        array $shipping_options = null,
        string $error_message = null
    ) {
        $args = [
            'shipping_query_id' => $shipping_query_id,
            'ok'                => $ok
        ];
        if ($shipping_options !== null) {
            $args['shipping_options'] = $shipping_options;
        }
        if ($error_message !== null) {
            $args['error_message'] = $error_message;
        }
        return $this->post('answerShippingQuery', $args);
    }

    public function answerPreCheckoutQuery(
        string $pre_checkout_query_id,
        bool $ok,
        string $error_message = null
    ) {
        $args = [
            'pre_checkout_query_id' => $pre_checkout_query_id,
            'ok'                    => $ok
        ];
        if ($error_message !== null) {
            $args['error_message'] = $error_message;
        }
        return $this->post('answerPreCheckoutQuery', $args);
    }

    public function setPassportDataErrors(
        int $user_id,
        $errors
    ) {
        $args = [
            'user_id' => $user_id,
            'errors'  => $errors
        ];
        return $this->post('setPassportDataErrors', $args);
    }

    public function sendGame(
        int $chat_id,
        string $game_short_name,
        bool $disable_notification = null,
        int $reply_to_message_id = null,
        array $reply_markup = null
    ) {
        $args = [
            'chat_id'         => $chat_id,
            'game_short_name' => $game_short_name
        ];
        if ($disable_notification !== null) {
            $args['disable_notification'] = $disable_notification;
        }
        if ($reply_to_message_id !== null) {
            $args['reply_to_message_id'] = $reply_to_message_id;
        }
        if ($reply_markup !== null) {
            $args['reply_markup'] = $reply_markup;
        }
        return $this->post('sendGame', $args);
    }

    public function setGameScore(
        int $user_id,
        int $score,
        bool $force = null,
        bool $disable_edit_message = null,
        int $chat_id = null,
        int $message_id = null,
        string $inline_message_id = null
    ) {
        $args = [
            'user_id' => $user_id,
            'score'   => $score
        ];
        if ($force !== null) {
            $args['force'] = $force;
        }
        if ($disable_edit_message !== null) {
            $args['disable_edit_message'] = $disable_edit_message;
        }
        if ($chat_id !== null) {
            $args['chat_id'] = $chat_id;
        }
        if ($message_id !== null) {
            $args['message_id'] = $message_id;
        }
        if ($inline_message_id !== null) {
            $args['inline_message_id'] = $inline_message_id;
        }
        return $this->post('setGameScore', $args);
    }
    public function getGameHighScores(
        int $user_id,
        int $chat_id = null,
        int $message_id = null,
        string $inline_message_id = null
    ) {
        $args = [
            'user_id' => $user_id
        ];
        if ($chat_id !== null) {
            $args['chat_id'] = $chat_id;
        }
        if ($message_id !== null) {
            $args['message_id'] = $message_id;
        }
        if ($inline_message_id !== null) {
            $args['inline_message_id'] = $inline_message_id;
        }
        return $this->post('getGameHighScores', $args);
    }
}
