<?php

namespace TGBotBase;

class Connection
{
    use Connection\MySQL;
    use Connection\Redis;
    use Connection\PostgreSQL;

}
