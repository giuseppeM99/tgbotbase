<?php

namespace TGBotBase\Connection;

trait MySQL
{
    private static $MySQL;

    public static function connectMySQL(bool $reconnect = false)
    {
        if (self::connectedMySQL()) {
            if (!$reconnect) return;
            self::$MySQL = null;
        }

        $dbname = \TGBotBase\Config::getConfig('MYSQL_DB');
        $dbuser = \TGBotBase\Config::getConfig('MYSQL_USER');
        $dbpass = \TGBotBase\Config::getConfig('MYSQL_PASS');
        $dbhost = \TGBotBase\Config::getConfig('MYSQL_HOST');
        $MySQL = new \PDO("mysql:dbname=$dbname;host=$dbhost", $dbuser, $dbpass);
        $MySQL->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
        self::$MySQL = $MySQL;
    }

    public static function getMySQL($forceConnect = false)
    {
        if ($forceConnect){
            self::connectMySQL(false);
        }
        return self::$MySQL;
    }

    public static function connectedMySQL()
    {
        return self::$MySQL !== null;
    }
}
