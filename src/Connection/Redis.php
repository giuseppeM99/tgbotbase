<?php

namespace TGBotBase\Connection;

trait Redis
{
    private static $Redis;

    public static function connectRedis(bool $reconnect = false)
    {
        if (self::$Redis !== null) {
            if (!$reconnect) return;
            self::$Redis = null;
        }

        $dbhost = \TGBotBase\Config::getConfig('REDIS_HOST');
        $dbname = \TGBotBase\Config::getConfig('REDIS_DB', true) ?: 0;
        $dbauth = \TGBotBase\Config::getConfig('REDIS_AUTH', true);


        self::$Redis = new \Redis();
        self::$Redis->connect($dbhost);
        if ($dbauth) {
            self::$Redis->auth($dbauth);
        }
        self::$Redis->select($dbname);

    }

    public static function getRedis()
    {
        self::connectRedis();
        return self::$Redis;
    }

    public static function connectedRedis()
    {
        return self::$Redis === null;
    }
}
