<?php

namespace TGBotBase\Connection;

trait PostgreSQL
{
    private static $PgSQL;

    public static function connectPgSQL(bool $reconnect = false)
    {
        if (self::connectedPgSQL()) {
            if (!$reconnect) return;
            self::$PgSQL = null;
        }

        $dbname = \TGBotBase\Config::getConfig('PGSQL_DB');
        $dbuser = \TGBotBase\Config::getConfig('PGSQL_USER');
        $dbpass = \TGBotBase\Config::getConfig('PGSQL_PASS');
        $dbhost = \TGBotBase\Config::getConfig('PGSQL_HOST');
        $PgSQL = new \PDO("pgsql:dbname=$dbname;host=$dbhost", $dbuser, $dbpass);
        $PgSQL->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
        self::$PgSQL = $PgSQL;
    }

    public static function getPgSQL($forceConnect = false)
    {
        if ($forceConnect){
            self::connectPgSQL(false);
        }
        return self::$PgSQL;
    }

    public static function connectedPgSQL()
    {
        return self::$PgSQL !== null;
    }
}
