<?php

namespace TGBotBase;

class Bot
{
    private $bot;
    private $onMessageCB;
    private $update;

    private $callbacks = [
        'message' => [],
        'callback_query' => [],
        'inline_query' => []
    ];

    private $preprocess = [
        'message' => [],
        'callback_query' => [],
        'inline_query' => []
    ];

    private $isWebhook = true;

    private static function parseUpdate($update)
    {
        return json_decode($update, true);
    }

    public function __construct($token)
    {
        $this->bot = new \TGBotBase\BotAPI\Bot($token);
    }

    private function getReplyID($update)
    {
        if (isset($update['message'])) {
            $fromID = $update['message']['from']['id'];
            $chatID = $update['message']['chat']['id'];
            $messageID = $update['message']['message_id'];

            if ($chatID === $this->getBot()->getID()) {
                $sendID = $fromID;
            } else {
                $sendID = $chatID;
            }
        } elseif (isset($update['callback_query'])) {
            $fromID = $update['callback_query']['from']['id'];
            $chatID = $update['callback_query']['message']['chat']['id'];
            $messageID = $update['callback_query']['message']['message_id'];
            $sendID = $chatID;
        }

        return [
            'fromID' => $fromID,
            'chatID' => $chatID,
            'sendID' => $sendID,
            'messageID' => $messageID
        ];
    }

    private function readPayload()
    {
        $payload = file_get_contents('php://input');
        $update = self::parseUpdate($payload);
        $this->update = $update;
    }

    public function getUpdate()
    {
        if ($this->isWebhook) {
            $this->readPayload();
        }

        return $this->update;
    }

    public function getBot()
    {
        return $this->bot;
    }

    public function initWebhookBot($bot)
    {
        self::$bot = $bot;
        self::readPayload();
    }

    public function onMessage($cb)
    {
        $this->onMessageCB = $cb;
    }

    public function onCommand(string $command, callable $cb, bool $stop = true)
    {
        return $this->onPattern("#^[/]${command}#i", $cb, $stop);
    }

    public function onPattern(string $pattern, callable $cb, bool $stop = true)
    {
        $this->callbacks['message'][] = [
            'pattern' => $pattern,
            'callback' => $cb,
            'stop' => $stop
        ];

        return $this;
    }

    public function onCallbackQuery(string $pattern, callable $cb, bool $stop = true)
    {
        $this->callbacks['callback_query'][] = [
            'pattern' => $pattern,
            'callback' => $cb,
            'stop' => $stop
        ];
    }

    public function preprocessMessage(callable $cb, int $priority = 0)
    {
        $this->preprocess['message'][] = [
            'callback' => $cb,
            'priority' => $priority
        ];

        usort($this->preprocess['message'], function($a, $b) {
            return $a['priority'] - $b['priority'];
        });

        return $this;
    }

    public function doPreprocessMessage(&$message)
    {
        foreach ($this->preprocess['message'] as $pp) {
            if (!$pp['callback']($message)) {
                return false;
            }
        }
        return true;
    }

    public function preprocessCallbackQuery(callable $cb, int $priority = 0)
    {
        $this->preprocess['callback_query'][] = [
            'callback' => $cb,
            'priority' => $priority
        ];

        usort($this->preprocess['callback_query'], function($a, $b) {
            return $a['priority'] - $b['priority'];
        });

        return $this;
    }

    public function doPreprocessCallbackQuery(&$message)
    {
        foreach ($this->preprocess['callback_query'] as $pp) {
            if (!$pp['callback']($message)) {
                return false;
            }
        }
        return true;
    }

    public function preprocessInlineQuery(callable $cb, int $priority = 0)
    {
        $this->preprocess['inline_query'][] = [
            'callback' => $cb,
            'priority' => $priority
        ];

        usort($this->preprocess['inline_query'], function($a, $b) {
            return $a['priority'] - $b['priority'];
        });

        return $this;
    }

    public function doPreprocessInlineQuery(&$message)
    {
        foreach ($this->preprocess['inline_query'] as $pp) {
            if (!$pp['callback']($message)) {
                return false;
            }
        }
        return true;
    }

    public function reply(string $text, array $replyMarkup = null)
    {
        $replyInfo = $this->getReplyID($this->update);

        return $this->getBot()->sendMessage($replyInfo['sendID'], $text, 'HTML', null, null, $replyInfo['messageID'], $replyMarkup);

        //return $this;
    }

    public function say(string $text, array $replyMarkup = null)
    {
        $replyInfo = $this->getReplyID($this->update);

        return $this->getBot()->sendMessage($replyInfo['sendID'], $text, 'HTML', null, null, null, $replyMarkup);

        //return $this;
    }

    public function edit(string $text, array $replyMarkup = null)
    {
        $replyInfo = $this->getReplyID($this->update);

        return $this->getBot()->editMessageText($replyInfo['sendID'], $replyInfo['messageID'], null, $text, 'HTML', null, $replyMarkup);

        //return $this;
    }


    public function editMarkup(array $replyMarkup)
    {
        $replyInfo = $this->getReplyID($this->update);

        return $this->getBot()->editMessageReplyMarkup($replyInfo['sendID'], $replyInfo['messageID'], null, $replyMarkup);

        //return $this;

    }

    public function acb(?string $text = '', bool $force = false, ?string $url = null)
    {
        if (isset($this->update['callback_query'])) {
            $cbid = $this->update['callback_query']['id'];
            return $this->getBot()->answerCallbackQuery($cbid, $text, $force, $url);
        }
    }

    public function run()
    {
        $update = $this->getUpdate();

        if (isset($update['message']) && $this->doPreprocessMessage($update['message'])) {
            $message = $update['message'];

            do {
                foreach ($this->callbacks['message'] as $pattern) {
                    if (preg_match($pattern['pattern'], $message['text'], $res)) {
                        $pattern['callback']($this, $res, $message);
                        if ($pattern['stop']) {
                            continue 2;
                        }
                    }
                }

                if (isset($this->onMessageCB)) {
                    ($this->onMessageCB)($this, $message);
                }
            } while (0);
        }

        if (isset($update['callback_query']) && $this->doPreprocessCallbackQuery($update['callback_query'])) {
            $query = $update['callback_query'];

            do {
                foreach ($this->callbacks['callback_query'] as $pattern) {
                    if (preg_match($pattern['pattern'], $query['data'], $res)) {
                        $pattern['callback']($this, $res, $query);
                        if ($pattern['stop']) {
                            continue 2;
                        }
                    }
                }

                if (isset($this->onCallbackQuery)) {
                    ($this->onCallbackQuery)($this, $query);
                }
            } while (0);
        }

        if (isset($update['inline_query']) && $this->doPreprocessInlineQuery($update['inline_query'])) {
            $query = $update['inline_query'];

            do {
                foreach ($this->callbacks['inline_query'] as $pattern) {
                    if (preg_match($pattern['pattern'], $query['data'], $res)) {
                        $pattern['callback']($this, $res, $query);
                        if ($pattern['stop']) {
                            continue 2;
                        }
                    }
                }

                if (isset($this->onInlineQuery)) {
                    ($this->onInlineQuery)($this, $query);
                }
            } while (0);
        }
    }
}
