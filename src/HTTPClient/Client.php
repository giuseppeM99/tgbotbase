<?php

namespace TGBotBase\HTTPClient;

use \Curl\Curl;

class Client
{
    private static $instance = null;

    private static function getSharedInstance()
    {
        self::initSharedInstance();
        return self::$instance;
    }

    private static function initSharedInstance()
    {
        if (self::$instance === null) {
            self::$instance = self::newInstance();
        }

        return self::$instance;
    }

    public static function newInstance()
    {
        $instance = new Curl();
        $instance->setDefaultJsonDecoder(true);
        return $instance;
    }

    private $baseURL;
    private $customInstance;

    public function __construct(string $baseURL = null)
    {
        $this->baseURL = $baseURL;
    }

    private function getInstance()
    {
        if (isset($this->customInstance)) {
            return $this->customInstance;
        }
        return self::getSharedInstance();
    }

    public function setCustomInstance($instance = null)
    {
        $this->customInstance = $instance;
    }

    private function getURL($path)
    {
        if (isset($this->baseURL)) {
            $url = $this->baseURL . $path;
        } else {
            $url = $path;
        }

        return $url;
    }

    public function get(string $path = '', array $params = [])
    {
        $url = $this->getURL($path);
        $curl = $this->getInstance();
        $curl->get($url, $params);

        if ($curl->isError()) {
            throw new Exception($curl->getErrorMessage(), $curl->getErrorCode());
        }

        return $curl->getResponse();
    }

    public function post(string $path = '', array $params = [])
    {
        $url = $this->getURL($path);
        $curl = $this->getInstance();
        $curl->post($url, $params);

        if ($curl->isError()) {
            throw new Exception($curl->getErrorMessage(), $curl->getErrorCode(), $curl->getResponse());
        }

        return $curl->getResponse();
    }

    public function __destruct()
    {
    }
}
