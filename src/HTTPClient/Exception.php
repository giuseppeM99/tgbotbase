<?php

namespace TGBotBase\HTTPClient;

class Exception extends \TGBotBase\Exception
{
    private $response;

    public function __construct($message, $code, $response)
    {
        $this->response = $response;
        parent::__construct("HTTP CLIENT ERROR: $message", $code);
    }

    public function getResponse()
    {
        return $this->response;
    }
}
