<?php

namespace TGBotBase\Config;

class Exception extends \TGBotBase\Exception
{
    public function __construct($option)
    {
        parent::__construct("OPTION $option NOT FOUND IN CONFIG");
    }
}
