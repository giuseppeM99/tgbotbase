<?php

namespace TGBotBase;

class Config
{
    private static $config = [
        'TELEGRAM_API_URL' => 'https://api.telegram.org/',
        'TELEGRAPH_API_URL' => 'https://api.telegra.ph/',
    ];

    public static function setConfig(string $option, $config)
    {
        self::$config[$option] = $config;
    }

    public static function getConfig(string $option, bool $noexception = false)
    {
        if (isset(self::$config[$option])) {
            return self::$config[$option];
        }

        if ($noexception) {
            return false;
        }

        throw new \TGBotBase\Config\Exception($option);
    }
}
