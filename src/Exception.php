<?php

namespace TGBotBase;

class Exception extends \Exception
{
    public function __construct($message = null, $code = 0, self $previous = null, $file = null, $line = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
