<?php

namespace TGBotBase\Telegraph;

class Request
{
    private static $baseClient;

    private static function getBaseClient()
    {
        self::initBaseClient();
        return self::$baseClient;
    }

    private static function initBaseClient()
    {
        if (self::$baseClient === NULL) {
            self::$baseClient = \TGBotBase\HTTPClient\Client::newInstance(\TGBotBase\Config::getConfig('TELEGRAPH_API_URL'));
        }
    }

    private static function checkParams($params)
    {
        foreach ($params as $k => &$v) {

            if (is_array($v)) {
                $v = \json_encode($v);
            }
        }

        return $params;
    }


    private $client;

    private function createClient()
    {
        $this->client = new \TGBotBase\HTTPClient\Client(\TGBotBase\Config::getConfig('TELEGRAPH_API_URL'));

        if (\TGBotBase\Config::getConfig('SHARED_INSTANCE', true)) {
            $this->client->setCustomInstance(self::getBaseClient());
        }
    }

    public function __construct()
    {
        $this->createClient();
    }

    public function get(string $path = '', array $params = [])
    {
        return $this->client->get($path, $params);
    }

    public function post(string $path = '', array $params = [])
    {
        return $this->client->post($path, self::checkParams($params));
    }

    public function __destruct()
    {
    }
}
