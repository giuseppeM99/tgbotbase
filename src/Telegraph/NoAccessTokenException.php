<?php

namespace TGBotBase\Telegraph;

class NoAccessTokenException extends \TGBotBase\Exception
{
    public function __construct()
    {
        parent::__construct("NO ACCESS TOKEN");
    }
}
