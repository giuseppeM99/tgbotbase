<?php

namespace TGBotBase\Telegraph;

class API extends Request {

    private ?string $access_token;

    public function __construct(?string $access_token = null)
    {
        $this->access_token = $access_token;
        parent::__construct();
    }

    public function setAccessToken(string $access_token)
    {
        $this->access_token = $access_token;
    }

    public function createAccount(string $short_name, string $author_name, string $author_url)
    {
        $args = [
            'short_name' => $short_name,
            'author_name' => $author_name,
            'author_url' => $author_url
        ];

        return $this->post(__FUNCTION__, $args);
    }

    public function getPage(string $path, bool $return_content = false)
    {
        $args = [
            'path' => $path,
            '$return_content' => $return_content
        ];

        return $this->post(__FUNCTION__, $args);
    }


    public function getViews(string $path, ?int $year, ?int $month, ?int $day, ?int $hour)
    {
        $args = [
            'path' => $path
        ];

        if (!empty($hour) && !empty($day) && !empty($month) && !empty($year)) {
            $args['hour'] = $hour;
        }

        if (!empty($day) && !empty($month) && !empty($year)) {
            $args['day'] = $day;
        }

        if (!empty($month) && !empty($year)) {
            $args['month'] = $month;
        }

        if (!empty($year)) {
            $args['year'] = $year;
        }

        return $this->post(__FUNCTION__, $args);
    }

    public function editAccountInfo(?string $short_name, ?string $author_name, ?string $author_url)
    {
        if (empty($this->access_token)) {
            throw new NoAccessTokenException();
        }
        $args = [
            'access_token' => $this->access_token
        ];

        if (!empty($short_name)) {
            $args['short_name'] = $short_name;
        }

        if (!empty($author_name)) {
            $args['author_name'] = $author_name;
        }

        if (!empty($author_url)) {
            $args['author_url'] = $author_url;
        }

        return $this->post(__FUNCTION__, $args);
    }

    public function getAccountInfo(array $fields = ['short_name','author_name','author_url'])
    {
        if (empty($this->access_token)) {
            throw new NoAccessTokenException();
        }
        $args = [
            'access_token' => $this->access_token,
            'fields' => $fields
        ];

        return $this->post(__FUNCTION__, $args);
    }

    public function revokeAccessToken()
    {
        if (empty($this->access_token)) {
            throw new NoAccessTokenException();
        }
        $args = [
            'access_token' => $this->access_token
        ];

        return $this->post(__FUNCTION__, $args);
    }

    public function createPage(string $title, array $content, ?string $author_name, ?string $author_url, ?bool $return_content = false)
    {
        if (empty($this->access_token)) {
            throw new NoAccessTokenException();
        }
        $args = [
            'access_token' => $this->access_token,
            'title' => $title,
            'content' => $content
        ];

        if (!empty($author_name)) {
            $args['author_name'] = $author_name;
        }

        if (!empty($author_url)) {
            $args['author_url'] = $author_url;
        }

        if (!empty($return_content)) {
            $args['return_content'] = $return_content;
        }

        return $this->post(__FUNCTION__, $args);
    }

    public function editPage(string $path, array $content, string $title, ?string $author_name, ?string $author_url, ?bool $return_content = false)
    {
        if (empty($this->access_token)) {
            throw new NoAccessTokenException();
        }
        $args = [
            'access_token' => $this->access_token,
            'title' => $title,
            'content' => $content,
            'path' => $path
        ];

        if (!empty($author_name)) {
            $args['author_name'] = $author_name;
        }

        if (!empty($author_url)) {
            $args['author_url'] = $author_url;
        }

        if (!empty($return_content)) {
            $args['return_content'] = $return_content;
        }

        return $this->post(__FUNCTION__, $args);
    }

    public function getPageList(int $offset = 0, int $limit = 50)
    {
        if (empty($this->access_token)) {
            throw new NoAccessTokenException();
        }
        $args = [
            'access_token' => $this->access_token,
            'offset' => $offset,
            'limit' => $limit
        ];

        return $this->post(__FUNCTION__, $args);
    }

}
